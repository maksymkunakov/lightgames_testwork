﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text m_OpenedPairsText;
    public GameObject m_LoadingPanel;

    public string m_OpenedPairsString = "Opened Pairs: ";

    public void OnGameStarted( )
    {
        if ( m_LoadingPanel )
        {
            m_LoadingPanel.gameObject.SetActive( false );
        }

        UpdateOpenedPairsText( );
    }

    public void UpdateOpenedPairsText()
    {
        if ( m_OpenedPairsText )
        {
            m_OpenedPairsText.text = m_OpenedPairsString + GameController.Instance.OpenedPairs.ToString( );
        }
    }

    private void Awake( )
    {
        if ( m_LoadingPanel )
        {
            m_LoadingPanel.gameObject.SetActive( true );
        }
    }
}
