﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using GC = GameController;

public class CardController : MonoBehaviour, IPointerClickHandler
{
    public CardInfo Info { get; private set; }

    private Image mImage;
    private Animator mAnimator;
    private RectTransform mTransform;

    public void Init( )
    {
        mImage = GetComponent<Image>( );
        mAnimator = GetComponent<Animator>( );
        mTransform = GetComponent<RectTransform>( );
    }

    public void SetInfo ( CardInfo aInfo )
    {
        Info = aInfo;

        if ( mImage )
        {
            mImage.sprite = Sprite.Create( aInfo.Texture, new Rect( 0, 0, aInfo.Texture.width, aInfo.Texture.height ), Vector2.zero );
        }
    }

    public void OnPointerClick( PointerEventData aEventData )
    {
        GC.Instance.Level.OnCardClick( this );
    }

    public void PlayHideAnim()
    {
        if ( mAnimator )
        {
            mAnimator.SetTrigger( "Hiding" );
        }
    }

    public void PlayShowAnim()
    {
        if ( mAnimator )
        {
            mAnimator.SetTrigger( "Showing" );
        }
    }

    public void PlayDisableAnim( )
    {
        if ( mAnimator )
        {
            mAnimator.SetTrigger( "Disabling" );
        }
    }

    public void ResetCard()
    {
        if ( mAnimator )
        {
            mAnimator.SetTrigger( "Reset" );
        }
    }
}
