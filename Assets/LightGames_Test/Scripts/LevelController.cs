﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using GC = GameController;

public class LevelController : MonoBehaviour
{
    public float m_StartShowingTime = 5.0f;
    public float m_CardAnimationTime = 1.0f;
    public float m_CardLockingTime = 1.0f;

    public List<CardController> m_Cards;

    private (CardController, CardController) mSelectedPair;

    private const int cCardsCount = 6;

    private bool mIsOpeningLocked = false;

    public void Init( )
    {
        foreach ( var card in m_Cards )
        {
            card.Init( );
        }
    }

    public void InitLevel( )
    {
        mIsOpeningLocked = true;

        var shuffledCards = GetShuffledList( m_Cards );
        var shuffledInfos = GetShuffledList( GC.Instance.Data.Cards );

        CardInfo cardInfo = null;

        for ( int i = 0; i < cCardsCount; i++ ) 
        {
            if ( i % 2 == 0)
            {
                cardInfo = shuffledInfos [ i / 2 ];
            }

            shuffledCards [ i ].ResetCard( );
            shuffledCards [ i ].SetInfo( cardInfo );
        }

        StartCoroutine( HideAllCards( m_StartShowingTime ) );
    }

    public void OnCardClick( CardController aCard )
    {
        if ( !mIsOpeningLocked )
        {
            if ( mSelectedPair.Item1 )
            {
                if ( aCard != mSelectedPair.Item1 )
                {
                    mSelectedPair.Item2 = aCard;
                    mSelectedPair.Item2.PlayShowAnim( );

                    if ( mSelectedPair.Item1.Info.Id == aCard.Info.Id )
                    {
                        StartCoroutine( RemovePair( m_CardLockingTime, (mSelectedPair.Item1, mSelectedPair.Item2) ) );
                    }
                    else
                    {
                        StartCoroutine( HidePair( m_CardLockingTime, (mSelectedPair.Item1, mSelectedPair.Item2) ) );
                    }

                    mIsOpeningLocked = true;
                    mSelectedPair.Item1 = mSelectedPair.Item2 = null;

                    StartCoroutine( UnlockOpening( m_CardLockingTime + m_CardAnimationTime ) );
                }
            }
            else
            {
                mSelectedPair.Item1 = aCard;
                mSelectedPair.Item1.PlayShowAnim( );
            }
        }
    }
    
    private List<T> GetShuffledList<T>( List<T> aListToShuffle )
    {
        var shuffledList = new List<T>( aListToShuffle );
        var objectsCount = shuffledList.Count;

        for ( int i = 0; i < objectsCount; i++ )
        {
            T temp = shuffledList [ i ];

            int randomIndex = Random.Range( i, objectsCount );

            shuffledList [ i ] = shuffledList [ randomIndex ];
            shuffledList [ randomIndex ] = temp;
        }

        return shuffledList;
    }

    private IEnumerator HideAllCards( float aDelay )
    {
        yield return new WaitForSeconds( aDelay );

        foreach ( var card in m_Cards )
        {
            card.PlayHideAnim( );
        }

        StartCoroutine( UnlockOpening( m_CardAnimationTime ) );
    }

    private IEnumerator HidePair( float aDelay, (CardController, CardController) aPair )
    {
        yield return new WaitForSeconds( aDelay );

        aPair.Item1.PlayHideAnim( );
        aPair.Item2.PlayHideAnim( );
    }

    private IEnumerator RemovePair( float aDelay, (CardController, CardController) aPair )
    {
        yield return new WaitForSeconds( aDelay );

        StartCoroutine( OnPairOpened( m_CardAnimationTime ) );

        aPair.Item1.PlayDisableAnim( );
        aPair.Item2.PlayDisableAnim( );
    }

    private IEnumerator UnlockOpening( float aDelay )
    {
        yield return new WaitForSeconds( aDelay );

        mIsOpeningLocked = false;
    }

    private IEnumerator OnPairOpened( float aDelay )
    {
        yield return new WaitForSeconds( aDelay );

        GC.Instance.OnPairOpened( );
    }
}
