﻿using UnityEngine;
using System.Collections.Generic;

public struct CardInfosJson
{
    public Dictionary<string, CardInfoJson> CardInfos;
}

public struct CardInfoJson
{
    public string Link;
}

public class CardInfo
{
    public string Id;
    public Texture2D Texture;
}