﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

using UnityEngine;
using UnityEngine.Networking;

using Newtonsoft.Json;

public class DataManager : MonoBehaviour
{
    public string m_JsonFileId;
    public List<CardInfo> Cards { get; private set; }

    private const string cDownloadTemplate = "https://drive.google.com/uc?export=download&id=";

    public void StartLoading()
    {
        StartCoroutine( GetRequest( cDownloadTemplate + m_JsonFileId ) );
    }

    private void InitCardsData( UnityWebRequest aData )
    {
        Cards = new List<CardInfo>( );

        var cardInfosJson = JsonConvert.DeserializeObject<CardInfosJson>( aData.downloadHandler.text );

        foreach ( var cardInfoPair in cardInfosJson.CardInfos )
        {
            var cardInfo = new CardInfo( );
            cardInfo.Id = cardInfoPair.Key;

            Cards.Add( cardInfo );

            StartCoroutine( LoadTexture( cardInfoPair.Value.Link, cardInfo ) );
        }
    }

    IEnumerator LoadTexture( string aUrl, CardInfo aInfo )
    {
        DownloadHandlerTexture textureHandler = new DownloadHandlerTexture( true );

        UnityWebRequest webRequest = new UnityWebRequest( aUrl );
        webRequest.downloadHandler = textureHandler;

        yield return webRequest.SendWebRequest( );

        if ( !( webRequest.isNetworkError || webRequest.isHttpError ) )
        {
            aInfo.Texture = textureHandler.texture;

            GameController.Instance.OnTextureLoaded( );
        }
    }

    IEnumerator GetRequest( string aUrl )
    {
        UnityWebRequest www = UnityWebRequest.Get( aUrl );

        yield return www.SendWebRequest( );

        if ( www.isNetworkError || www.isHttpError )
        {
            Debug.LogError( www.error );
        }
        else
        {
            InitCardsData( www );
        }
    }
}
