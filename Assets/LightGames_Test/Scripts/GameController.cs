﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance = null;

    public int OpenedPairs { get; private set; } = 0;

    public DataManager Data { get; private set; }
    public UIController UI { get; private set; }
    public LevelController Level { get; private set; }

    private const int cPairsCount = 3;

    public void OnTextureLoaded()
    {
        bool isAllTextures = true;

        foreach ( var cardInfo in Data.Cards ) 
        {
            if ( cardInfo.Texture == null )
            {
                isAllTextures = false;
                break;
            }
        }

        if ( isAllTextures )
        {
            StartGame( );
        }
    }

    public void OnPairOpened()
    {
        OpenedPairs++;

        if ( UI )
        {
            UI.UpdateOpenedPairsText( );
        }

        if ( OpenedPairs >= cPairsCount )
        {
            StartGame( );
        }
    }

    private void Awake( )
    {
        if ( Instance == null )
        {
            Instance = this;
            DontDestroyOnLoad( gameObject );

            Init( );
        }
        else
        {
            Destroy( gameObject );
        }
    }

    private void Init()
    {
        Data = GetComponent<DataManager>( );

        if ( Data )
        {
            Data.StartLoading( );
        }

        Level = GetComponent<LevelController>( );

        if ( Level )
        {
            Level.Init( );
        }

        UI = GetComponent<UIController>( );
    }

    private void StartGame()
    {
        OpenedPairs = 0;

        UI.OnGameStarted( );
        Level.InitLevel( );
    }
}
